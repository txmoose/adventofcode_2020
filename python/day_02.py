def main():
    password_list = []
    with open("prompts/password_list.txt", "r") as pl:
        for line in pl.readlines():
            password_list.append(line.split())
    
    valid_count = 0

    for pw in password_list:
        first, second = pw[0].split('-')
        first = int(first) - 1
        second = int(second) - 1
        req = pw[1].strip(':')
        try:
            if pw[2][first] == req and pw[2][second] != req:
                valid_count += 1
            elif pw[2][first] != req and pw[2][second] == req:
                valid_count += 1
        
        except IndexError:
            continue
    
    print(f'{valid_count=}')


if __name__ == "__main__":
    main()
