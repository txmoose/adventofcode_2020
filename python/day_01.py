def main():
    expense_report = []
    with open("prompts/expense_report.txt", "r") as er:
        for line in er.readlines():
            expense_report.append(int(line))

    go_again = True
    while go_again:
        first = expense_report.pop(0)
        second = -1

        for index in range(len(expense_report)):
            for index_2 in range(1, len(expense_report)):
                if first + expense_report[index] >= 2020:
                    break

                if first + expense_report[index] + expense_report[index_2] == 2020:
                    second = expense_report[index]
                    third = expense_report[index_2]
                    go_again = False
                    break

    print(f'{first=} {second=} {third=}')
    print(f'{first * second * third}')


if __name__ == '__main__':
    main()
