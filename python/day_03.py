def find_trees(slope: list) -> int:
    hillside = []
    with open("prompts/map.txt", "r") as map_file:
        for line in map_file.readlines():
            hillside.append(line.strip())
    
    x_travel, y_travel = slope
    tree_count = 0
    x_y = [0, 0]
    wrap = len(hillside[0])

    while True:
        # If X is beyond the end of the map, wrap back to the beginning
        x_y[0] = (x_y[0] + x_travel) % wrap
        
        # If we're below the map, we made it, so break the loop
        if x_y[1] + y_travel >= len(hillside):
            break

        # Otherwise, set new Y coordinate
        x_y[1] += y_travel

        if hillside[x_y[1]][x_y[0]] == '#':
            tree_count += 1

    print(f'{slope=} - {tree_count=}')
    return tree_count


def main():
    slopes = [
        [1, 1],
        [3, 1],
        [5, 1],
        [7, 1],
        [1, 2]
    ]

    trees_counts = []

    for slope in slopes:
        trees_counts.append(find_trees(slope))

    tree_sum = 1
    for count in trees_counts:
        tree_sum *= count

    print(f"{tree_sum=}")

if __name__ == "__main__":
    main()
