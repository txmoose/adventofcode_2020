def main():
    group_answers = []
    group = []
    new_group = True
    with open("prompts/declaration_answers.txt", "r") as ans:
        for line in ans.readlines():
            if line == "\n":
                group_answers.append(len(group))
                group = []
                new_group = True
                continue

            if new_group:
                group += list(line.strip())
                new_group = False
            else:
                group = list(set(group).intersection(list(line.strip())))

        group_answers.append(len(group))
    
    print(f"Total \"yes\" answers: {sum(group_answers)}")


if __name__ == "__main__":
    main()
