import re

def main():
    VALID_KEYS = [
        'ecl',
        'pid',
        'eyr',
        'hcl',
        'byr',
        'iyr',
        'hgt',
        'cid'
        ]
    VALID_KEYS_TMP = [
        'ecl',
        'pid',
        'eyr',
        'hcl',
        'byr',
        'iyr',
        'hgt'
        ]
    passport_batch = []
    ppt = {}
    valid_count = 0
    with open("prompts/passports.txt", "r") as ppts:
        for line in ppts.readlines():
            if line == "\n":
                passport_batch.append(ppt)
                ppt = {}
                continue

            for item in line.split():
                key, value = item.split(':')
                ppt[key] = value

        passport_batch.append(ppt)

    print(f'{len(passport_batch)=}')

    for ppt in passport_batch:
        if set(VALID_KEYS) == set(ppt.keys()) or set(VALID_KEYS_TMP) == set(ppt.keys()):
            if len(ppt['byr']) != 4 or not 1920 <= int(ppt['byr']) <= 2002:
                continue

            if len(ppt['iyr']) != 4 or not 2010 <= int(ppt['iyr']) <= 2020:
                continue

            if len(ppt['eyr']) != 4 or not 2020 <= int(ppt['eyr']) <= 2030:
                continue

            if not re.search("^\d+(cm|in)$", ppt['hgt']):
                continue

            if re.search("^\d+cm$", ppt['hgt']) and not 150 <= int(ppt['hgt'][:-2]) <= 193:
                continue
            elif re.search("^\d+in$", ppt['hgt']) and not 59 <= int(ppt['hgt'][:-2]) <= 76:
                continue

            if not re.search("^#[0-9a-f]{6}$", ppt['hcl']):
                continue

            if ppt['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
                continue

            if not re.search('^\d{9}$', ppt['pid']):
                continue

            valid_count += 1
    
    print(f"{valid_count=}")


if __name__ == "__main__":
    main()