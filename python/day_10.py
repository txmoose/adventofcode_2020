def main():
    outlet = 0
    adapters = [outlet]
    with open("prompts/adapters.txt", "r") as adprs:
        for adpr in adprs.readlines():
            adapters.append(int(adpr.strip()))
        
    device_rating = max(adapters) + 3
    adapters.append(device_rating)
    adapters.sort()

    one_step = 0
    two_step = 0
    thr_step = 0

    for i in range(1, len(adapters)):
        print(f"Adapter Joltage - {adapters[i]}")
        diff = adapters[i] - adapters[i - 1]
        if diff == 1:
            one_step += 1
        elif diff == 2:
            two_step += 1
        elif diff == 3:
            thr_step += 1
    
    print(f"{one_step=} * {thr_step=} = {one_step * thr_step}")


if __name__ == "__main__":
    main()
