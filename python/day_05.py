def main():
    seats = {}
    all_seat_ids = []
    with open("prompts/seats.txt", "r") as sf:
        for line in sf.readlines():
            seats[line.strip()] = None

    for seat in seats.keys():
        seats[seat] = int("0b" + seat.replace("B", "1").replace("F", "0").replace("R", "1").replace("L", "0"),
                      base=2)

    all_seat_ids = sorted(seats.values())
    for ndx in range(8, len(all_seat_ids)):
        if all_seat_ids[ndx] + 1 == all_seat_ids[ndx + 1]:
            continue

        print(f'{all_seat_ids[ndx] + 1}')
        break


if __name__ == "__main__":
    main()