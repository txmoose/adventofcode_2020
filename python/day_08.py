def main():
    with open("prompts/opcode.txt", "r") as opcode:
        master_instructions = opcode.readlines()

    not_found = True
    already_attempted = []

    while not_found:
        instructions = master_instructions[:]
        accumulator = 0
        address = 0
        prev_address = 0
        visited = []
        this_round_changed = True

        while True:
            if address == len(instructions):
                print(f"{accumulator=}")
                not_found = False
                break

            if instructions[address].split()[0] == "nop" and address not in already_attempted and this_round_changed:
                instructions[address] = f"jmp {instructions[address].split()[1]}"
                already_attempted.append(address)
                this_round_changed = False
            elif instructions[address].split()[0] == "jmp" and address not in already_attempted and this_round_changed:
                instructions[address] = f"nop {instructions[address].split()[1]}"
                already_attempted.append(address)
                this_round_changed = False

            inst, arg = instructions[address].split()
            if address in visited:
                print(f"Changing {prev_address} didn't work.")
                break

            visited.append(address)

            if inst == "nop":
                prev_address = address
                address += 1
            elif inst == "acc":
                accumulator += int(arg)
                prev_address = address
                address += 1
            elif inst == "jmp":
                prev_address = address
                address += int(arg)
            else:
                print(f"{inst=} is bad input")
                break




if __name__ == "__main__":
    main()
