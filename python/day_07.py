import re
import json

rules = {}
running_count = 0
re_search = "(\d+)\s(\w+\s\w+)\sbags*"


def main():
    with open("prompts/bag_rules.txt", "r") as rules_txt:
        for line in rules_txt.readlines():
            rule = line.strip().split(" contain ")
            inner = re.findall(re_search, line)
            inner_dict = {}
            if inner:
                for child in inner:
                    inner_dict[child[1]] = child[0]
            else:
                inner_dict["none"] = 0

            rules[rule[0][:-5]] = inner_dict

    print(json.dumps(rules, indent=4))

    running_count = contained_bags("shiny gold")
    print(f"{running_count=}")


def contained_bags(color):
    bag_count = 0
    child_bags = rules[color]
    for child, count in child_bags.items():
        count = int(count)
        if child == "none":
            return bag_count

        bag_count += count
        bag_count += (count * contained_bags(child))
    return bag_count


if __name__ == "__main__":
    main()
