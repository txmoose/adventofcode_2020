def main():
    xmas_cipher = []

    with open("prompts/xmas_cipher.txt", "r") as cipher:
        for line in cipher.readlines():
            xmas_cipher.append(int(line.strip()))

    PREAMBLE_LENGTH = 25
    potentials = xmas_cipher[:PREAMBLE_LENGTH]

    for i in range(PREAMBLE_LENGTH, len(xmas_cipher)):
        target = xmas_cipher[i]
        if find_pairs(potentials[:], target):
            potentials.pop(0)
            potentials.append(target)
        else:
            break
    print(f"{target=}")

    counter = 0
    for i in range(len(xmas_cipher)):
        j = i
        still_less = True
        while still_less:
            if counter < target:
                counter += xmas_cipher[j]
                j += 1
                continue
            elif counter == target:
                print(f"{min(xmas_cipher[i:j-1])=}")
                print(f"{max(xmas_cipher[i:j-1])=}")
                print(f"{min(xmas_cipher[i:j-1]) + max(xmas_cipher[i:j-1])=}")
                raise SystemExit
            elif counter > target:
                counter = 0
                still_less = False


def find_pairs(potential_lst, target):
    for i in potential_lst:
        if target - i in potential_lst and target - i != i:
            return True
        else:
            continue
    else:
        return False


if __name__ == "__main__":
    main()
